# Working with Viv and AVIVATOR
___
- testing build and deploy of VIV and AVIVIATOR
- see [hms-dbmi viv](https://github.com/hms-dbmi/viv/tree/master) for more information

___

## NODE.JS prerequisites
- VIV and AVIVATOR are built with node.js and it is critical to install the correct version of node

### Install `nvm` to manage node versions
- `brew install nvm` # install nvm with brew
- `mkdir ~/.nvm` # make nvm root directory
- add the following to .zshrc config file (make sure nvm paths are correct - run `brew info nvm` for more information)
    ```
      #-------------------------------------
      # NVM setup
      #-------------------------------------
      export NVM_DIR="$HOME/.nvm" [ -s "/opt/homebrew/opt/nvm/nvm.sh" ] && \. "/opt/homebrew/opt/nvm/nvm.sh"  # This loads nvm
    [ -s "/opt/homebrew/opt/nvm/etc/bash_completion.d/nvm" ] && \. "/opt/homebrew/opt/nvm/etc/bash_completion.d/nvm"  # This loads nvm bash_completion
    ```
- `nvm install 18.6.0` # install specified node version
- `nvm use 18.6.0` set node version as default
- `nvm list` to see list of available node
- `npm --version` check npm version - should be 8.13.2
- `npm install -g pnpm` install pnpm
- `pnpm --version` check pnpm version - should be 8.5.1

- ___NOTE___ - these versions are the ones used to successfully build the apps

## Install and Build VIV and AVIVATOR
- Create new locally directory to clone to (e.g. `<user>/dev/3-VIV-001`)
- `git clone https://github.com/hms-dbmi/viv.git && cd viv` - clone repository and change to `viv` directory
  - ___NOTE___  the repo was cloned at commit 
    ```
    commit 2b28cc1db6ad1dacb44e6b1cd145ae90c46a2ef3 (HEAD -> master, origin/master, origin/HEAD)
    Author: Nathan Drezner <38958867+ndrezn@users.noreply.github.com>
    Date:   Tue May 2 04:47:58 2023 -0400
    ```
- Run `pnpm install` in `viv` root folder to install viv and avivator
- Run `pnpm build` to build all packages, avivator, and documentation
  - ___NOTE___ `pnpm -r build --filter=avivator` did not work and errored due to directory creation issues

    
## Test local webserving of AVIVATOR
### Setup node http server
- See https://timnwells.medium.com/quick-and-easy-http-servers-for-local-development-7a7df5ac25ff for more information
- `npm install http-server -g` install node http-server 
- navigate to `/dev/3-VIV-001/viv/sites/avivator/dist` folder
- `http-server . -p 8000` run node http-server from current directory
- navigate to chrome browser `localhost:8000`
- use example ome.zarr links from IDR group https://idr.github.io/ome-ngff-samples/
  - ___NOTE___ not all file types work - e.g. plate datasets do not work 
- Dragging and dropping local .zarr files does not work 

### Test local serving of .zarr files for AVIVATOR

#### Download .zarr files from IDR
- setup aws cli, if do not currently have it installed https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html
- run `aws --no-sign-request --endpoint-url=https://uk1s3.embassy.ebi.ac.uk s3 sync s3://idr/zarr/v0.3/9836842.zarr  ~/dev/3-VIV-001/data`
  - --no-sign-request is needed as no login is required
  - --endpoint-url is needed as location is hosted s3 location
  - download as many datasets as want to test 

#### Setup node serving of images
- navigatge to `~/dev/3-VIV-001/data`
- `http-server --cors='*' -p 9000 .` 
  - cors is necessary for serving files via http - see https://github.com/hms-dbmi/viv/tree/master/sites/docs/tutorial
- test loading local zarr file (e.g. `http://localhost:9000/9836842.zarr`) in avivator running in localhost:8000

## Prebuilt AVIVATOR
- If the above is not working, a prebuilt distribution of AVIVATOR was released with v0.6.0 of VIV
  - see https://forum.image.sc/t/viv-and-avivator/45999/12? for more information
- to test the prebuilt distribution
  - create local directory folder for the viv package (e.g. 3-VIV-002)
  - `npm pack @hms-dbmi/viv@0.6.0`
  - `tar zxvf hms-dbmi-viv-0.6.0.tgz`
  - navigate to the unpacked `package/dist/` folder and proceed as described in [local webserving of AVIVATOR](#test-local-webserving-of-avivator)
  - ___NOTE___ this is a older version of avivator as the current version tested in the above code was 0.13.7





# Host a Static Webpage with Content from Gitlab

### Launch an EC2 Instance
- Login to [aws](https://aws-sso-vrtx.awsapps.com/start#/)
- go to `Management Console`
- search for `EC2`
- in the **EC2 Dashboard**, click `Launch Instance`, and then click Launch instance
- **Name and Tag** box click Add additional tags
  - In the **Key** input box, type or select **Application**
  - In the **Value** input box, name your Application
  - In Resource types: select **Instances** and **Volumes**
    ![here](README-images/Name_Tag_1_1.png )


#### Application and OS Images (Amazon Machine Image)
 
- Select the machine image you would like to use: In our case we used the default one as it is a basic site. we used `Amazon Linux 2 AMI (HVM), SSD Volume Type`
  ![here](README-images/Application_and_OS_Images_1_1.png)

#### Instance Type 

- Select the instance type (machine resources). We selected: **t2.micro**

  ![here](README-images/Instance_Type_1_1.png)

#### Key Pair (login)

- to cretae key pair:![here](README-images/to_cretae_key_pair_1_1.png)
- It will download a file `ChosenName.pem`, save this to `.ssh` folder.

#### Network settings

- as it is a basic site we will only select **SSH Traffic** and **HTTP Traffic** 
  ![here](README-images/Network_settings_1_1.png)

#### Select Launch Instance 

- if successful it will show ![here](README-images/Select_Launch_Instance.png)

#### re-direct to the EC2 Dashboard

- Click the **check box next to your instance**, and click **Connect** at the top-ish of the page

- it will be re-directed to the **Connect to instance** page

  ![here](README-images/Connect_to_instance_1_1.png)

### SSH into the EC2 Instance and Install a Web Server

- for our case we will connect with `ssh client`
- to connect it is important to put the `ChosenName.pem` file in a secure location. In this case we put the file in `.ssh` folder.
- go to terminal and navigate to the folder where the `ChosenName.pem`, in our case navigate to `.ssh` folder
- run `chmod 400 ChosenName.pem`
- next run `ssh -i "ChosenName.pem" ec2-user@ip` # Type `yes` to continue.
At this point, your terminal is now interacting directly with your EC2 instance (aka your �virtual laptop�), rather than your physical machine. For example: 

![for example](README-images/Connect_with-SSH.png)

- run `sudo su`
- run `yum update -y` to update all of the packages on the instance
- run `yum install httpd -y` to install an apache webserver
- run `service httpd start` to start the webserver
- run `chkconfig httpd on` to configure the web server to restart if it gets stopped

### Install Git on the EC2 Instance
- At this point you should still be in your EC2 instance. Install git with the following command:`yum install git -y`
- Navigate to the directory `.ssh` if you are not already

### Generate your SSH keys on the EC2 instance

- Run the following commands to generate your SSH keys on the EC2 instance: `ssh-keygen -t rsa -C "akter@vrtx.com"`
- press `Enter`
- run `cat ~/.ssh/id_rsa.pub` to see the key
- manually put the SSH key to your gitlab and generate the key
- navigate to `/var/www/html`
- clone the repository, run `git clone repository link`
- by running `ls` you should see the repository in the directory
- To get the contents of /viv/sites/avivator/dist into the /var/www/html folder i have run: `sudo cp -R 3-VIV-001/viv/sites/avivator/dist/* .`
- Navigate back to the EC2 dashboard in the AWS console and and copy the Public/ private DNS(IPV4) of your instance into your clipboard. Paste the address into your browser. You should see the app!
